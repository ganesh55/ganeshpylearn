import pandas as pd

my_df = pd.DataFrame({
    'Person': ['Alice', 'Steven', 'Neesham', 'Chris', 'Alice'],
    'City': ['Berlin', 'Montreal', 'Toronto', 'Rome', 'Munich'],
    'Mother Tongue': ['German', 'French', 'English', 'Italian', 'German'],
    'Age':  [37, 20, 38, 23, 35],

},index=["A","B","C","D","E"])

df_reset=my_df.reset_index(drop=True)

print("Before reseting Index:")
print(my_df,"\n")

print("After reseting Index:")
print(df_reset)
