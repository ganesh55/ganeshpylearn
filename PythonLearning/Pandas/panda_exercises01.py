import pandas as pd
import numpy as np
print(pd.__version__)
print(pd.show_versions(as_json=True))
mylist = list('abcdefghijklmnopqrstuvwxyz');
print(mylist)
myarr = np.arange(26)
print(myarr)
mydict = dict(zip(myarr,mylist))
print(mydict)

print ("hello")
ser1 = pd.Series(mylist)
print(ser1)
print ("series of array")
ser2 = pd.Series(myarr)
print(ser2)
print ("series of dict only head")

ser3 = pd.Series(mydict)
print(ser3.head())
print ("series of dict full")

print(ser3)
